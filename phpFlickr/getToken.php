<?php
/* Last updated with phpFlickr 1.4
 *
 * If you need your app to always login with the same user (to see your private
 * photos or photosets, for example), you can use this file to login and get a
 * token assigned so that you can hard code the token to be used.  To use this
 * use the phpFlickr::setToken() function whenever you create an instance of 
 * the class.
 */

 /* Modified for Drupal */

// bootstrap drupal
chdir('../../../../../');
require_once('./includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

    require_once("phpFlickr.php");
    $key = variable_get('flickr_api_key', '');
    $secret = variable_get('flickr_api_secret', '');
    $f = new phpFlickr($key, $secret);
    
    //change this to the permissions you will need
    $f->auth("write");
    
    variable_set('flickr_api_token', $_SESSION['phpFlickr_auth_token']);
    
    drupal_set_message(t('Token set.'));
    drupal_goto('http://'. $_SERVER['SERVER_NAME'] .'/admin/settings/flickr');
    
    //echo "Copy this token into your code: " . $_SESSION['phpFlickr_auth_token'];
    