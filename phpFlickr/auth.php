<?php
/* Last updated with phpFlickr 1.3.1
 *
 * Edit these variables to reflect the values you need. $default_redirect 
 * and $permissions are only important if you are linking here instead of
 * using phpFlickr::auth() from another page or if you set the remember_uri
 * argument to false.
 */
 
 /* Modified for Drupal */

// bootstrap drupal
chdir('../../../../../');
require_once('./includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

    $path = drupal_get_path('module', 'flickr_imagefield');
    $api_key                 = variable_get('flickr_api_key', '');
    $api_secret              = variable_get('flickr_api_secret', '');
    $default_redirect        = "./getToken.php";
    $permissions             = "write";
    $path_to_phpFlickr_class = $path .'/phpFlickr/';

    ob_start();
    require_once($path_to_phpFlickr_class . "phpFlickr.php");
    unset($_SESSION['phpFlickr_auth_token']);
     
	if (!empty($_GET['extra'])) {
		$redirect = $_GET['extra'];
	}
    
    $f = new phpFlickr($api_key, $api_secret);
 
    if (empty($_GET['frob'])) {
        $f->auth($permissions, false);
    } else {
        $f->auth_getToken($_GET['frob']);
	}
    
    if (empty($redirect)) {
		header("Location: " . $default_redirect);
    } else {
		header("Location: " . $redirect);
    }
 
?>